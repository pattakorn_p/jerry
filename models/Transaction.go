package models

type Transaction struct {
	Id         int32 `json:"id" bson:"_id"`
	Bank       string `json:"bank" bson:"bank"`
	Txid       string `json:"txid" bson:"txid"`
	Time       int32 `json:"time" bson:"time"`
	Channel    string `json:"channel" bson:"channel"`
	Value      float32 `json:"value" bson:"value"`
	Fee        float32 `json:"fee" bson:"fee"`
	Detail     string `json:"detail" bson:"detail"`
	Checktime  int32 `json:"checktime" bson:"checktime"`
	Tx_Hash    string `json:"tx_hash" bson:"tx_hash"`
	Raw_Hash   string `json:"raw_hash" bson:"raw_hash"`
	Status     string `json:"status" bson:"status"`
	Trigger    string `json:"trigger" bson:"trigger"`
	Tranferer  string `json:"tranferer" bson:"tranferer"`
	Atranferer string `json:"atranferer" bson:"atranferer"`
}

type TransactionBody struct {
	Id         string `json:"id" bson:"_id"`
	Bank       string `json:"bank" bson:"bank"`
	Txid       string `json:"txid" bson:"txid"`
	Time       string `json:"time" bson:"time"`
	Channel    string `json:"channel" bson:"channel"`
	Value      string `json:"value" bson:"value"`
	Fee        string `json:"fee" bson:"fee"`
	Detail     string `json:"detail" bson:"detail"`
	Checktime  string `json:"checktime" bson:"checktime"`
	Tx_Hash    string `json:"tx_hash" bson:"tx_hash"`
	Raw_Hash   string `json:"raw_hash" bson:"raw_hash"`
	Status     string `json:"status" bson:"status"`
	Trigger    string `json:"trigger" bson:"trigger"`
	Tranferer  string `json:"tranferer" bson:"tranferer"`
	Atranferer string `json:"atranferer" bson:"atranferer"`

}
