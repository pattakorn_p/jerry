package routes

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"jerry/handlers"
	"jerry/middlewares"
	"net/http"
)

func Route (e *echo.Echo){

	middlewares.SetCORS(e)
	e.Use(middleware.Recover())

	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, echo.Map{
			"status": "OK",
			"message": "Hello, World!",
		})
	})

	e.GET("/switch", handlers.GetSwitch)
	e.GET("/deposit", handlers.GetTransactions)
	e.GET("/withdraw", handlers.GetTransactions)

	e.GET("/summary", handlers.GetRTSummary)
	e.GET("/summary/daily", handlers.GetDailySummary)

	e.GET("/profit/daily", handlers.GetDailyProfit)
	e.GET("/profit/weekly", handlers.GetWeeklyProfit)
	e.GET("/profit/monthly", handlers.GetMonthlyProfit)

	e.POST("/receiver", handlers.Receive)
}