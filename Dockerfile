FROM golang:1.13.5-alpine AS builder

WORKDIR /usr/src/jerry
COPY . .

RUN apk add build-base
RUN go build jerry

###

FROM alpine:3.9.5
COPY configs/.env /configs/.env
COPY --from=builder /usr/src/jerry/jerry /go/bin/jerry
EXPOSE 1323
CMD ["/go/bin/jerry"]