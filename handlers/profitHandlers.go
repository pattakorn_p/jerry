package handlers

import (
	"context"
	_ "encoding/json"
	_ "fmt"
	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	_ "go.mongodb.org/mongo-driver/mongo"
	"jerry/configs"
	"jerry/models"
	_ "jerry/models"
	"net/http"
	"os"
	"time"
)

func GetDailyProfit(c echo.Context) error {

	mdb, err := configs.ConnectMDB()
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "Error",
			"message": err,
		})
	}

	collection := mdb.Collection(os.Getenv("MDB_COLLECTION"))

	var returnSlice []interface{}
	//var calcSlice []models.Transaction

	dayStep := int64(86400)
	today := time.Date(
		time.Now().Year(),
		time.Now().Month(),
		time.Now().Day(),
		0,
		0,
		0,
		0,
		time.UTC).Unix()

	startDate := today
	var endDate int64
	for i := 0;i < 30;i += 1{
		endDate = startDate + 86399
		var results []models.Transaction
		cursor, err := collection.Find(context.Background(), bson.D{
			{"atranferer", bson.D{{"$ne", "463595809"}}},
			{"time", bson.D{{"$gte", startDate}}},
			{"time", bson.D{{"$lte", endDate}}},
		})
		if err != nil{
			return c.JSON(http.StatusBadRequest, echo.Map{
				"status": "Error",
				"message": err,
			})
		}
		cursor.All(context.Background(), &results)
		cursor.Close(context.Background())
		returnSlice = append(returnSlice, echo.Map{
			"start": time.Unix(startDate, 0).UTC(),
			"end": time.Unix(endDate, 0).UTC(),
			"data": configs.GetTransBalance(results),
		})
		startDate -= dayStep
	}

	return c.JSON(http.StatusOK, echo.Map{
		"status": "message",
		"data": echo.Map{
			"data": returnSlice,
		},
	})
}
func GetWeeklyProfit(c echo.Context) error {

	mdb, err := configs.ConnectMDB()
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "Error",
			"message": err,
		})
	}

	collection := mdb.Collection(os.Getenv("MDB_COLLECTION"))

	var returnSlice []interface{}
	//var calcSlice []models.Transaction

	tn  := time.Now().UTC()
	year, week := tn.ISOWeek()
	weekTimeUnix := configs.FirstDayOfISOWeek(year, week, time.UTC).Unix() - 604800

	var weekendTime time.Time
	for i := 0;i < 6;i += 1{
		weekTime := time.Unix(weekTimeUnix, 0).UTC()
		weekendTime = time.Unix(weekTimeUnix, 0).AddDate(0,0,6).Add(time.Hour*23+time.Minute*59+time.Second*59).UTC()
		weekendTimeUnix := weekendTime.Unix()

		var results []models.Transaction
		cursor, err := collection.Find(context.Background(), bson.D{
			{"atranferer", bson.D{{"$ne", "463595809"}}},
			{"time", bson.D{{"$gte", weekTimeUnix}}},
			{"time", bson.D{{"$lte", weekendTimeUnix}}},
		})
		if err != nil{
			return c.JSON(http.StatusBadRequest, echo.Map{
				"status": "Error",
				"message": err,
			})
		}
		cursor.All(context.Background(), &results)
		cursor.Close(context.Background())
		returnSlice = append(returnSlice, echo.Map{
			"start": weekTime,
			"end": weekendTime,
			"data": configs.GetTransBalance(results),
		})
		weekTimeUnix = time.Unix(weekTimeUnix, 0).AddDate(0,0,-7).Unix()
	}

	return c.JSON(http.StatusOK, echo.Map{
		"status": "message",
		"data": echo.Map{
			"data": returnSlice,
		},
	})
}
func GetMonthlyProfit(c echo.Context) error {

	mdb, err := configs.ConnectMDB()
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "Error",
			"message": err,
		})
	}

	collection := mdb.Collection(os.Getenv("MDB_COLLECTION"))

	year, month, _ := time.Now().Date()
	firstOfMonth := time.Date(year, month, 1, 0, 0, 0, 0, time.UTC)


	var returnSlice []interface{}

	for i := 0;i < 6;i += 1{
		lastOfMonth := firstOfMonth.AddDate(0, 1, -1)
		fMonthUnix := firstOfMonth.Unix()
		lMonthUnix := lastOfMonth.Unix()

		var results []models.Transaction
		cursor, err := collection.Find(context.Background(), bson.D{
			{"atranferer", bson.D{{"$ne", "463595809"}}},
			{"time", bson.D{{"$gte", fMonthUnix}}},
			{"time", bson.D{{"$lte", lMonthUnix}}},
		})
		if err != nil{
			return c.JSON(http.StatusBadRequest, echo.Map{
				"status": "Error",
				"message": err,
			})
		}
		cursor.All(context.Background(), &results)
		cursor.Close(context.Background())
		returnSlice = append(returnSlice, echo.Map{
			"start": firstOfMonth,
			"end": lastOfMonth,
			"data": configs.GetTransBalance(results),
		})
		firstOfMonth = firstOfMonth.AddDate(0,-1, 0)
	}

	return c.JSON(http.StatusOK, echo.Map{
		"status": "message",
		"data": echo.Map{
			"data": returnSlice,
		},
	})
}
