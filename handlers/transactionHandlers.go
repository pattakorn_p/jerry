package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"io/ioutil"
	"jerry/configs"
	"jerry/models"
	"math"
	"net/http"
	"os"
	_ "os"
	"strconv"
	"strings"
	"time"
)

func GetSwitch(c echo.Context) error {

	path := configs.TrimPageQuery(c.Request().RequestURI)

	mdb, err := configs.ConnectMDB()
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}
	coll := mdb.Collection(os.Getenv("MDB_COLLECTION"))

	from := c.QueryParam("from") + "T00:00:00Z"
	to := c.QueryParam("to") + "T00:00:00Z"

	fromDate, err := time.Parse(time.RFC3339, from)
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}
	toDate, err := time.Parse(time.RFC3339, to)
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}

	fromUnix := fromDate.Unix()
	toUnix := toDate.Unix() + 86399

	var result []models.Transaction

	limit := int64(15)
	var page int64
	if c.QueryParam("page") != "" {
		page, _ = strconv.ParseInt(c.QueryParam("page"), 10, 32)
	} else {
		page = 1
	}

	selectQuery := bson.D{
		{"time", bson.D{{"$gte", fromUnix}}},
		{"time", bson.D{{"$lte", toUnix}}},
		{"atranferer", "463595809"},
	}
	total, err := coll.CountDocuments(context.Background(), selectQuery)
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"_id", -1}})
	findOptions.SetSkip(limit * (page - 1))
	findOptions.SetLimit(limit)
	cursor, err := coll.Find(context.Background(), selectQuery, findOptions)
	defer cursor.Close(context.Background())
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}

	err = cursor.All(context.Background(), &result)
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}
	lastPageFloat := math.Ceil(float64(total)/float64(limit))
	lastPage := strconv.FormatFloat(lastPageFloat, 'f', 0, 32)
	var prevUrl string
	if page != 1{
		prevUrl = path + "&page=" + strconv.FormatInt(page-1, 10)
	}
	var nextUrl string
	if page != int64(lastPageFloat){
		nextUrl = path + "&page=" + strconv.FormatInt(page+1, 10)
	}

	return c.JSON(http.StatusOK, echo.Map{
		"status":  "OK",
		"data": echo.Map{
			"current_page":   page,
			"from":           (limit * (page - 1)) + 1,
			"to":             page * limit,
			"path":           path,
			"next_page_url":  nextUrl,
			"last_page_url":  path + "&page=" + lastPage,
			"first_page_url": path + "&page=1",
			"prev_page_url":  prevUrl,
			"total":          total,
			"per_page":       limit,
			"last_page":      lastPage,
			"data":           result,
		},
	})
}


func GetTransactions(c echo.Context) error {

	path := configs.TrimPageQuery(c.Request().RequestURI)

	mdb, err := configs.ConnectMDB()
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}
	coll := mdb.Collection(os.Getenv("MDB_COLLECTION"))

	date := c.QueryParam("date") + "T00:00:00Z"
	dateTime, err := time.Parse(time.RFC3339, date)
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}

	dateUnix := dateTime.Unix()
	endDateUnix := dateUnix + 86399

	var result []models.Transaction

	var bsonQuery bson.D

	if strings.Contains(path, "deposit") {
		bsonQuery = bson.D{
			{"atranferer", bson.D{{"$ne", "463595809"}}},
			{"time", bson.D{{"$gte", dateUnix}}},
			{"time", bson.D{{"$lte", endDateUnix}}},
			{"value", bson.D{{"$gt", 0.00}}},
		}
	} else if strings.Contains(path, "withdraw") {
		bsonQuery = bson.D{
			{"atranferer", bson.D{{"$ne", "463595809"}}},
			{"time", bson.D{{"$gte", dateUnix}}},
			{"time", bson.D{{"$lte", endDateUnix}}},
			{"value", bson.D{{"$lt", 0.00}}},
		}
	}

	limit := int64(15)
	var page int64
	if c.QueryParam("page") != "" {
		page, _ = strconv.ParseInt(c.QueryParam("page"), 10, 32)
	} else {
		page = 1
	}

	total, err := coll.CountDocuments(context.Background(), bsonQuery)
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{"_id", -1}})
	findOptions.SetSkip(limit * (page - 1))
	findOptions.SetLimit(limit)
	cursor, err := coll.Find(context.Background(), bsonQuery, findOptions)

	//cursor, err := coll.Find(context.Background(), bsonQuery)
	defer cursor.Close(context.Background())
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}

	err = cursor.All(context.Background(), &result)
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}
	lastPageFloat := math.Ceil(float64(total)/float64(limit))
	lastPage := strconv.FormatFloat(lastPageFloat, 'f', 0, 32)
	var prevUrl string
	if page != 1{
		prevUrl = path + "&page=" + strconv.FormatInt(page-1, 10)
	}
	var nextUrl string
	if page != int64(lastPageFloat){
		nextUrl = path + "&page=" + strconv.FormatInt(page+1, 10)
	}
	return c.JSON(http.StatusOK, echo.Map{
		"status":  "OK",
		"data": echo.Map{
			"current_page":   page,
			"from":           (limit * (page - 1)) + 1,
			"to":             page * limit,
			"path":           path,
			"next_page_url":  nextUrl,
			"last_page_url":  path + "&page=" + lastPage,
			"first_page_url": path + "&page=1",
			"prev_page_url":  prevUrl,
			"total":          total,
			"per_page":       limit,
			"last_page":      lastPage,
			"data":           result,
		},
	})

}

func Receive(c echo.Context) error {

	mdb, err := configs.ConnectMDB()
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}

	collection := mdb.Collection(os.Getenv("MDB_COLLECTION"))

	body, err := ioutil.ReadAll(c.Request().Body)
	defer fmt.Println(string(body))
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}

	trans, err := prepareBody(body)
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status":  "fail",
			"message": err,
		})
	}
	var ids []interface{}

	for _, t := range trans {
		result, err := collection.InsertOne(context.Background(), t)
		if err != nil {
			return c.JSON(http.StatusBadRequest, echo.Map{
				"status":  "fail",
				"message": err,
			})
		}
		ids = append(ids, result.InsertedID)
	}

	return c.JSON(http.StatusOK, echo.Map{
		"status": "success",
		"data": echo.Map{
			"data": ids,
		},
	})
}

func prepareBody(body []byte) ([]models.Transaction, error) {
	var objects []models.TransactionBody
	var returnObj []models.Transaction

	err := json.Unmarshal(body, &objects)
	if err != nil {
		panic(err)
	}

	for _, el := range objects {
		newId, err := strconv.ParseInt(el.Id, 10, 64)
		if err != nil {
			return nil, err
		}
		newTime, err := strconv.ParseInt(el.Time, 10, 64)
		if err != nil {
			return nil, err
		}
		newCheckTime, err := strconv.ParseInt(el.Checktime, 10, 64)
		if err != nil {
			return nil, err
		}
		newValue, err := strconv.ParseFloat(el.Value, 64)
		if err != nil {
			return nil, err
		}
		newFee, err := strconv.ParseFloat(el.Fee, 64)
		if err != nil {
			return nil, err
		}
		converted := models.Transaction{
			Id:         int32(newId),
			Bank:       el.Bank,
			Txid:       el.Txid,
			Time:       int32(newTime),
			Channel:    el.Channel,
			Value:      float32(newValue),
			Fee:        float32(newFee),
			Detail:     el.Detail,
			Checktime:  int32(newCheckTime),
			Tx_Hash:    el.Tx_Hash,
			Raw_Hash:   el.Raw_Hash,
			Status:     el.Status,
			Trigger:    el.Trigger,
			Tranferer:  el.Tranferer,
			Atranferer: el.Atranferer,
		}
		returnObj = append(returnObj, converted)
	}

	return returnObj, nil
}
