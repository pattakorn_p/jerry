package handlers

import (
	"context"
	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"jerry/configs"
	"jerry/models"
	"net/http"
	"os"
	"time"
)

func GetRTSummary(c echo.Context) error {
	mdb, err := configs.ConnectMDB()
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "fail",
			"message": err,
		})
	}
	collection := mdb.Collection(os.Getenv("MDB_COLLECTION"))
	var trans []models.Transaction
	_, err = time.LoadLocation("Asia/Bangkok")
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "fail",
			"message": err,
		})
	}

	today := time.Now()
	todayUnix := time.Date(
		today.Year(),
		today.Month(),
		today.Day(),
		0,
		0,
		0,
		0,
		time.UTC).Unix()
	todayEnd := todayUnix + 86399

	cursor, err := collection.Find(context.Background(), bson.D{
		{"atranferer", bson.D{{"$ne", "463595809"}}},
		{"time",bson.D{{"$gte", todayUnix}}},
		{"time",bson.D{{"$lte", todayEnd}}},
	})
	defer cursor.Close(context.Background())
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "fail",
			"message": err,
		})
	}

	err = cursor.All(context.Background(), &trans)
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "fail",
			"message": err,
		})
	}
	result := configs.GetTransBalance(trans)

	return c.JSON(http.StatusOK, echo.Map{
		"status": "OK",
		"data": echo.Map{
			"date": today.Local(),
			"data": result,
		},
	})
}
//
func GetDailySummary(c echo.Context) error {

	mdb, err := configs.ConnectMDB()
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "fail",
			"message": err,
		})
	}
	collection := mdb.Collection(os.Getenv("MDB_COLLECTION"))
	var trans []models.Transaction
	_, err = time.LoadLocation("Asia/Bangkok")
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "fail",
			"message": err,
		})
	}
	today := time.Now()
	yesterday := time.Date(
		today.Year(),
		today.Month(),
		today.Day(),
		0,
		0,
		0,
		0,
		time.UTC).Unix() - 86400
	yesterdayEnd := yesterday + 86399

	cursor, err := collection.Find(context.Background(), bson.D{
		{"atranferer", bson.D{{"$ne", "463595809"}}},
		{"time",bson.D{{"$gte", yesterday}}},
		{"time",bson.D{{"$lte", yesterdayEnd}}},
	})
	defer cursor.Close(context.Background())
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "fail",
			"message": err,
		})
	}

	err = cursor.All(context.Background(), &trans)
	if err != nil{
		return c.JSON(http.StatusBadRequest, echo.Map{
			"status": "fail",
			"message": err,
		})
	}
	result := configs.GetTransBalance(trans)

	return c.JSON(http.StatusOK, echo.Map{
		"status": "OK",
		"data": echo.Map{
			"date": time.Unix(yesterday,0).UTC(),
			"data": result,
		},
	})
}
