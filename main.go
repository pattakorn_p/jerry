package main

import (
	"github.com/labstack/echo/v4"
	"jerry/configs"
	"jerry/routes"
	"net/http"
	"time"
)

func main(){
	configs.Init()
	e := echo.New()
	routes.Route(e)

	s := &http.Server{
		Addr:":1323",
		ReadTimeout: 30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout: 120 * time.Second,
	}

	e.Logger.Fatal(e.StartServer(s))
}
