package configs

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"jerry/models"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"time"
)

func Init(){
	path, err := filepath.Abs("./configs/.env")
	if err != nil{log.Fatal(err)}
	err = godotenv.Load(path)
	if err != nil{log.Fatal(err)}
}

func ConnectMDB() (*mongo.Database, error) {
	connectionString := fmt.Sprintf("mongodb://%s:%s@%s:%s/%s?authSource=%s",
		os.Getenv("MDB_USERNAME"),
		os.Getenv("MDB_PASSWORD"),
		os.Getenv("MDB_HOST"),
		os.Getenv("MDB_PORT"),
		os.Getenv("MDB_NAME"),
		os.Getenv("MDB_AUTH_SOURCE"),
	)

	db, err := mongo.Connect(context.Background(), options.Client().ApplyURI(connectionString))
	if err != nil{
		return  nil, err
	}
	return db.Database(os.Getenv("MDB_NAME")), nil
}

func GetTransBalance(trans []models.Transaction) float32 {
	var balance float32
	for _, el := range trans{
		balance += el.Value
	}
	return balance
}

func FirstDayOfISOWeek(year int, week int, timezone *time.Location) time.Time {
	date := time.Date(year, 0, 0, 0, 0, 0, 0, timezone)
	isoYear, isoWeek := date.ISOWeek()

	// iterate back to Monday
	for date.Weekday() != time.Monday {
		date = date.AddDate(0, 0, -1)
		isoYear, isoWeek = date.ISOWeek()
	}

	// iterate forward to the first day of the first week
	for isoYear < year {
		date = date.AddDate(0, 0, 7)
		isoYear, isoWeek = date.ISOWeek()
	}

	// iterate forward to the first day of the given week
	for isoWeek < week {
		date = date.AddDate(0, 0, 7)
		isoYear, isoWeek = date.ISOWeek()
	}

	return date
}

func TrimPageQuery(path string) string{

	url, _ := url.Parse(os.Getenv("HOST_PATH") + path)
	values := url.Query()
	values.Del("page")
	url.RawQuery = values.Encode()

	return url.String()
}